<?php
/**
 * Created by PhpStorm.
 * User: riyan
 * Date: 10/19/2016
 * Time: 2:23 PM
 */

namespace Custom;
use Tgallice\Wit\ConverseApi;
use Tgallice\Wit\Model\Context;

use Tgallice\Wit\Client;
use Tgallice\Wit\MessageApi;
use Tgallice\Wit\Helper;


class MyConverseApi extends ConverseApi
{
    public $client;
    public $wit_ai='HVR463NJELPM2GVGMIO6KMIRXASD6MAO';
    function __construct($client)
    {
        parent::__construct($client);
        $this->client = $client;
    }

    /**
     * @param string $sessionId
     * @param string|null $text
     * @param Context|null $context
     *
     * @return array
     */
    public function converse($sender, $text = null, Context $context = null)
    {
        $query = [
            'session_id' => $sender,
        ];

        if (!empty($text)) {
            $query['q'] = $text;
        }

        $response = $this->client->send('POST', '/converse', $context, $query);
        $response = $this->decodeResponse($response);

        $ci =& get_instance();

        //$ci->load->model('MAIN/chatbot_m');
        $ci->load->library('Curl');

        $url="http://localhost:3000/ducked";
        $ci->curl->create($url);
        $post = array('text'=>$text);
        $ci->curl->post($post);

        $result=$ci->curl->execute();
        $result=json_decode($result);




        if(!empty($result))
        {
            foreach($result as $key=>$row) {
                $value[$key] = $row->{'value'};
                if($value[$key]->{'unit'} == 'bedroom')
                {
                    $duckling['bedroom']=array(
                        'type'=>$value[$key]->{'type'},
                        'value'=>$value[$key]->{'value'},
                    );
                }
                else if($value[$key]->{'unit'} == 'bathroom')
                {
                    $duckling['bathroom']=array(
                        'type'=>$value[$key]->{'type'},
                        'value'=>$value[$key]->{'value'},
                    );
                }
                else if($value[$key]->{'unit'} == 'IDR')
                {
                    $duckling['property_price']=array(
                        'type'=>$value[$key]->{'type'},
                        'value'=>$value[$key]->{'value'},
                    );
                }
            }

            foreach($duckling as $key=>$row)
            {
                $response['entities'][$key]=array(
                    array(
                        'type'=>$row['type'],
                        'value'=>$row['value'],
                    )
                );
            }
        }
        return $response;
    }
}