<?php

namespace Tgallice\WitDemo\Action;

use Tgallice\Wit\ActionMapping;
use Tgallice\Wit\Helper;
use Tgallice\Wit\Model\Context;
use Tgallice\Wit\Model\Step;

use pimax\Messages\Message;
use pimax\Messages\StructuredMessage;
use pimax\Messages\MessageElement;
use pimax\Messages\ImageMessage;
use pimax\Messages\MessageButton;

use pimax\FbBotApp;

class MyActionMapping extends ActionMapping
{
    private $output;

    public $fb_access_token = 'EAAExSHnApDsBABxdSUuxJAneDutShQBEQdC7fQuOWeOScE5Yo3H72VNLutPtdwyt6Ao3SNcJIv0AeFvrGZCOUarOijMVbRAaSgrJL5EiShoLxMREnCP1jjIQtImZCLOk1AckA0ZC9hUVKJV6eRmWiy6PbIHNjHi4tehrnTrmwZDZD';

    public function __construct($social)
    {
        $this->social=$social;
        $this->CI=&get_instance();


    }

    /**
     * @inheritdoc
     */
    public function action($sessionId, $actionName, Context $context, array $entities = [])
    {
        $this->CI->load->driver('cache', array('adapter' => config_item('cache_adapter'), 'backup' => config_item('cache_backup')));

        if ($actionName === 'getProperties') {
            $this->cache_name='chat/'.$sessionId;
            $cache_data=$this->CI->cache->get($this->cache_name)==NULL?array():$this->CI->cache->get($this->cache_name);



            if($this->CI->cache->get($this->cache_name)!=NULL) {
                $ent = array_merge($cache_data,$entities);
            } else {
                $ent = $entities;
            }
            $ent['property_getlimit']=3;
            $ent['property_getstart']=0;

            $this->CI->cache->save($this->cache_name,$ent,86400);


            $cache_data=$this->CI->cache->get($this->cache_name);

            $context = $this->getProperties($context, $cache_data);


        } else if($actionName === 'getmoreProperties') {

            $this->cache_name='chat/'.$sessionId;

            $cache_data=$this->CI->cache->get($this->cache_name)==NULL?array():$this->CI->cache->get($this->cache_name);

            if($this->CI->cache->get($this->cache_name)!=NULL) {
                $ent = array_merge($cache_data,$entities);
            } else {
                $ent = $entities;
            }

            if(isset($ent['property_getlimit'])) {
                $ent['property_getlimit'] = $ent['property_getlimit']+3;
                $ent['property_getstart'] = $ent['property_getlimit']-3;
            } else {
                $ent['property_getlimit'] = 3;
                $ent['property_getstart'] = 0;
            }

            $this->CI->cache->save($this->cache_name,$ent,86400);

            $cache_data=$this->CI->cache->get($this->cache_name);

            $context = $this->getProperties($context, $cache_data);
        } else if($actionName === 'getShortlisting') {
            $context = $this->getShortlisting($context, $entities);
        } elseif($actionName === 'getPropertiesNews') {
            $context = $this->getPropertiesNews($context, $entities);
        } elseif($actionName === 'getPopularNews') {
            $context = $this->getPopularNews($context, $entities);
        } elseif($actionName === 'getGreeting') {
            $context = $this->getGreeting($context, $entities);
        } else if($actionName === 'deleteCache') {
            $this->CI->cache->delete('/chat/'.$sessionId);
            $context = $this->deleteCache($sessionId);
        }

        return $context;
    }

    /**
     * @inheritdoc
     */

    public function say($sessionId, $message, Context $context, array $entities = [])
    {
        //load chat_bot_m
        //chatbot_m->send($message,$this->social);
        //echo $message;
        if($this->social == 'fb') {
            if((is_string($message) && (is_object(json_decode($message)) || is_array(json_decode($message))))) {
                $messages = json_decode($message);
                $action = $messages->{'action'};

                if($action=='getProperties' || $action=='getmoreProperties') {

                    $message = $messages->{'result_array'};
                    $where = $messages->{'where'};

                    foreach($message as $key=>$row)
                    {
                        $new_data['template_data'][$key][]=
                            new MessageElement($row->{'productname'}.' - '.number_format($row->{'price'}),
                                $row->{'district'}.'-'.$row->{'state'}, $row->{'cover'},null,$row->{'url'});
                    }

                    $new_data['status']=TRUE;
                    $new_data['type']='template';
                    $new_data['action'] = $action;
                } else if($action=='getShortlisting') {

                    $message = $messages->{'result_array'};

                    foreach($message as $row)
                    {
                        $new_data['template_data'][]=
                            new MessageElement($row->{'short_s'}.' - '.$row->{'category'}, $row->{'district'}.'-'.$row->{'state'}, null,
                                [
                                    new MessageButton(MessageButton::TYPE_WEB, 'Selengkapnya', $row->{'url'}),
                                ]);
                    }

                    $new_data['status']=TRUE;
                    $new_data['type']='template';
                    $new_data['action'] = $action;

                } else if($action == 'getPropertiesNews') {
                    $message = $messages->{'news'};

                    foreach($message as $row)
                    {
                        $new_data['template_data'][]=
                            new MessageElement($row->{'topic'}, $row->{'category'}, $row->{'avatar'},
                                [
                                    new MessageButton(MessageButton::TYPE_WEB, 'Selengkapnya', $row->{'url'}),
                                ]);
                    }

                    $new_data['status']=TRUE;
                    $new_data['type']='template';
                    $new_data['action'] = $action;

                } else if($action == 'getPopularNews') {
                    $message = $messages->{'news'};

                    foreach($message as $row)
                    {
                        $new_data['template_data'][]=
                            new MessageElement($row->{'topic'}, $row->{'category_desc'}, $row->{'avatar'},
                                [
                                    new MessageButton(MessageButton::TYPE_WEB, 'Selengkapnya', $row->{'url'}),
                                ]);
                    }

                    $new_data['status']=TRUE;
                    $new_data['type']='template';
                    $new_data['action'] = $action;
                } else if($action == 'getGreeting') {

                        $new_data['template_data'][]=
                            new MessageElement('Rumahdewi.com', 'Jual Beli Sewa Properti Rumah Apartemen Tanah', null,
                                [
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Cari Properti'),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Iklan Singkat'),
                                    new MessageButton(MessageButton::TYPE_POSTBACK, 'Berita Terbaru'),
                                ]);

                    $new_data['status']=TRUE;
                    $new_data['type']='template';
                    $new_data['action'] = $action;
                }
            } else {
                $new_data['message']= $message;
                $new_data['status']=FALSE;
                $new_data['type']='text';
            }

            $bot = new FbBotApp($this->fb_access_token);

            $result=array();
            switch($new_data['type'])
            {
                case 'text':
                    $result=new Message($sessionId, $new_data['message']);
                    $bot->send($result);
                    break;

                case 'template':
                    if($new_data['action']=='getGreeting') {
                        $message=new Message($sessionId, 'Hai, Nama saya rumahdewi-chatbot, Saya dengan senang hati akan menjawab pertanyaan Anda di rumahdewi. Tanyakan, apa yang ingin Anda ketahui mengenai properti?');

                        $bot->send($message);
                    }


                    if($new_data['action']=='getProperties' || $new_data['action']=='getmoreProperties') {

                       foreach($new_data['template_data'] as $key=>$row) {

                           $result = new StructuredMessage($sessionId,
                               StructuredMessage::TYPE_GENERIC,
                               [
                                   'elements'=>$row
                               ]
                           );
                           $bot->send($result);
                           if($key==2) break;
                       }

                    } else {
                        $result = new StructuredMessage($sessionId,
                            StructuredMessage::TYPE_GENERIC,
                            [
                                'elements' => $new_data['template_data']
                            ]
                        );
                        $bot->send($result);
                    }

                    break;
            }



            if($new_data['type']=='template')
            {
                if($new_data['action']=='getProperties' || $new_data['action']=='getmoreProperties')
                {
                    $reset_message = new Message($sessionId, 'Ketik "next" untuk data berikutnya. Ketik "reset" Untuk megulang pencarian anda yang sebelumnya.');
                    $bot->send($reset_message);
                }
            }
            kill($result,true);
            return $result;
        } else if($this->social == 'telegram') {

        }

    }

    /**
     * @inheritdoc
     */
    public function error($sessionId, Context $context, $error = '', array $step = null)
    {
    }

    /**
     * @inheritdoc
     */
    public function merge($sessionId, Context $context, array $entities = [])
    {
        return $context;
    }

    /**
     * @inheritdoc
     */
    public function stop($sessionId, Context $context)
    {
        //$this->output->writeln('<info>+ Stop</info>');
    }

    private function getGreeting(Context $context, array $entities = [])
    {
        $result['action']='getGreeting';
        return new Context(['greeting' => json_encode($result)]);
    }

    private function getProperties(Context $context, array $entities = [])
    {

        $loc = Helper::getFirstEntityValue('property_location', $entities);
        $price = Helper::getFirstEntityValue('property_price', $entities);
        $category = Helper::getFirstEntityValue('property_category', $entities);
        $type = Helper::getFirstEntityValue('price_type', $entities);
        $listingfor = Helper::getFirstEntityValue('property_listingfor', $entities);

        if (!$loc) {
            return new Context(['missingLocation' => true]);
        }

        if (!isset($category)) {
            return new Context(['missingCategory' => true]);
        }
        $this->CI->load->model('USER/listing_m');
        if(isset($type))
        {
            $price_type = $type=='diatas'?'minprice':'maxprice';
        }
        else
        {
            $price_type = 'maxprice';
        }
        $listing_for = isset($listingfor)?$listingfor:1;
        $where = array(
            'keyword'=>$loc,
            'listingfor'=>$listing_for,
            $price_type=>$price,
            'category'=>$category
        );
        $result=$this->CI->listing_m->get_listings2($where,$entities['property_getlimit'],$entities['property_getstart']);
        $result['where'] = $where;
        $result['action']='getProperties';

        return new Context(['properties' =>  json_encode($result)]);
    }

    private function getShortlisting(Context $context, array $entities = [])
    {
        $this->CI->load->model('MAIN/short_listing_m');
        $result=$this->CI->short_listing_m->get_short_listings2(6,0);
        $result['action']='getShortlisting';
        return new Context(['properties' =>  json_encode($result)]);
    }

    private function getPropertiesNews(Context $context, array $entities = [])
    {
        $this->CI->load->model('MAIN/news_m');
        $result=$this->CI->news_m->news_get(6,0);
        $result['action']='getPropertiesNews';
        return new Context(['properties' =>  json_encode($result)]);
    }

    private function getPopularNews(Context $context, array $entities = [])
    {
        $this->CI->load->model('MAIN/news_m');
        $result['news']=$this->CI->news_m->most_view(6,0);
        $result['action']='getPopularNews';
        return new Context(['properties' =>  json_encode($result)]);
    }

    private function deleteCache($cache_name)
    {
        $result['result_array'] = 'oops';
        $result['action']='deleteCache';
        return new Context(['delete' => json_encode($result)]);
    }
}
