<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function kill($object,$print_r=FALSE)
{
	echo '<pre>';
	if($print_r==TRUE)
	{
		print_r($object);
	}
	else
	{
		var_dump($object);
	}
	echo '</pre>';

	die();
}